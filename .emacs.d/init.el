;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Configure package manager
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq package-check-signature nil)
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("gnu" . "https://elpa.gnu.org/packages/")))
(package-initialize)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Auto install use-package if it isn't already installed
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; USE-PACKAGE Modes and stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package exec-path-from-shell
  :ensure t
  :config
  (exec-path-from-shell-initialize))

(use-package diminish
  :ensure t)

(use-package helm
  :ensure t
  :diminish helm-mode
  :bind
  (;("M-x" . helm-M-x)
  :map helm-map
  ("<escape>" . helm-keyboard-quit))
  :config
  (helm-mode 1)
  (define-key global-map "\C-x\ \C-r" 'helm-mini)
  (define-key global-map [remap find-file] 'helm-find-files)
  (define-key global-map [remap occur] 'helm-occur)
  (define-key global-map [remap list-buffers] 'helm-buffers-list)
  (define-key global-map [remap switch-to-buffer] 'helm-buffers-list)
  (define-key global-map [remap dabbrev-expand] 'helm-dabbrev)
  (define-key global-map [remap execute-extended-command] 'helm-M-x)
  (define-key global-map [remap apropos-command] 'helm-apropos)
  (unless (boundp 'completion-in-region-function)
    (define-key lisp-interaction-mode-map [remap completion-at-point] 'helm-lisp-completion-at-point)
    (define-key emacs-lisp-mode-map       [remap completion-at-point] 'helm-lisp-completion-at-point)))

(use-package helm-rtags
  :ensure t
  :config
  (define-key c-mode-base-map (kbd "M-.")
    (function rtags-find-symbol-at-point))
  (define-key c-mode-base-map (kbd "M-,")
    (function rtags-find-references-at-point))
  (rtags-enable-standard-keybindings)
  ;; comment this out if you don't have or don't use helm
  (setq rtags-use-helm t))

(use-package flycheck-rtags
  :ensure t)
  
;; Spiffy mode line skin
(use-package powerline
  :ensure t
  :defer t
  :init
  (powerline-default-theme))

(use-package anzu
  :ensure t
  :diminish anzu-mode
  :config
  (global-anzu-mode +1)
  (global-set-key [remap query-replace] 'anzu-query-replace)
  (global-set-key [remap query-replace-regexp] 'anzu-query-replace-regexp))

(use-package magit
  :ensure t
  :bind
  ("C-x g" . magit-status)
  ("C-c m b" . magit-blame)
  ("C-c m d" . magit-diff-buffer-file)
  :config
  (magit-add-section-hook 'magit-status-sections-hook
                          'magit-insert-unpushed-to-upstream
                          'magit-insert-unpushed-to-upstream-or-recent
                          'replace))

;;(use-package lsp-mode
;;  :init
;;  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
;;  (setq lsp-keymap-prefix "C-c l")
;;  :ensure t
;;  :hook ((go-mode . lsp))
;;  :hook ((python-mode . lsp))
;;  ;; if you want which-key integration
;;  ;; (lsp-mode . lsp-enable-which-key-integration))
;;  :commands lsp
;;  :config
;;  (defun lsp-go-install-save-hooks ()
;;	(add-hook 'before-save-hook #'lsp-format-buffer t t)
;;	(add-hook 'before-save-hook #'lsp-organize-imports t t))
;;  (add-hook 'go-mode-hook #'lsp-go-install-save-hooks)
;;  (lsp-register-custom-settings
;;   '(("gopls.completeUnimported" t t)
;;	 ("gopls.staticcheck" t t))))

;;(use-package lsp-ui
;;  :ensure t
;;  :commands lsp-ui-mode)

;;(use-package helm-lsp
;;  :ensure t
;;  :commands helm-lsp-workspace-symbol)

(use-package bash-completion
  :ensure t
  :config
  (autoload 'bash-completion-dynamic-complete 
    "bash-completion"
    "BASH completion hook")
  (add-hook 'shell-dynamic-complete-functions
	    'bash-completion-dynamic-complete))

(use-package xterm-color
  :ensure t
  :config
  (setq comint-output-filter-functions
	(remove 'ansi-color-process-output comint-output-filter-functions))
  (add-hook 'shell-mode-hook
	    (lambda ()
	      ;; Disable font-locking in this buffer to improve performance
	      (font-lock-mode -1)
	      ;; Prevent font-locking from being re-enabled in this buffer
	      (make-local-variable 'font-lock-function)
	      (setq font-lock-function (lambda (_) nil))
	      (add-hook 'comint-preoutput-filter-functions 'xterm-color-filter nil t))))

(use-package shackle
  :ensure t
  :config
  (progn
    (setq shackle-rules
          '(
            ("*shell*"  :same t :select t)
            ("*grep*" :same t :select t)
            ("*compilation*" :same t :select t)
            ("*Help*" :popup t :select t)
            ))
    (shackle-mode 1)))

(use-package projectile
  :ensure t
  :diminish projectile-mode
  :config
  (projectile-mode +1)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Usep-PACKAGE Languages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(use-package protobuf-mode
  :ensure t
  :defer t)
(use-package csharp-mode
  :ensure t
  :defer t)
(use-package nix-mode
  :ensure t
  :defer t)
(use-package markdown-mode
  :ensure t
  :defer t)
(use-package yaml-mode
  :ensure t
  :defer t)
(use-package lua-mode
  :ensure t
  :defer t)
(use-package cmake-mode
  :ensure t
  :defer t)
(use-package dockerfile-mode
  :ensure t
  :defer t)
(use-package glsl-mode
  :ensure t
  :defer t)
(use-package json-mode
  :ensure t
  :defer t)
(use-package matlab-mode
  :mode ("\\.m\\'" . matlab-mode)
  :ensure t
  :defer t)
(use-package go-mode
  :ensure t
  :defer t)
(use-package js2-mode
  :mode ("\\.js\\'" . js2-mode)
  :ensure t
  :defer t)
(use-package web-mode
  :ensure t
  :defer t
  :mode ("\\.html\\'"
         "\\.css\\'"
         "\\.php\\'")
  :config
  (progn
    (setq web-mode-code-indent-offset 2)
    (setq web-mode-enable-auto-quoting nil)))

(use-package cuda-mode
  :ensure t
  :defer t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Alist configs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(add-to-list 'auto-mode-alist '("\\.usf$" . glsl-mode))
(add-to-list 'auto-mode-alist '("\\.ush$" . glsl-mode))
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; CXX-Mode settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq-default c-basic-offset 4)
(setq-default tab-width 4)
(setq-default indent-tabs-mode nil)
(add-hook 'c++-mode-hook (lambda () (which-function-mode 1)))
(modify-syntax-entry ?_ "w")
(modify-syntax-entry ?- "w")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Custom keybindings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key (kbd "C-x i") 'imenu)
(global-set-key (kbd "C-x f") 'switch-to-buffer)
(global-set-key (kbd "C-x t") 'toggle-truncate-lines)
(global-set-key (kbd "C-x /") 'string-insert-rectangle)
(global-set-key (kbd "RET")   'newline-and-indent)
(global-set-key (kbd "C-a")   'back-to-indentation)
(global-set-key (kbd "C-x m") 'matlab-shell)
(global-set-key (kbd "C-M-c") 'compile)
(global-set-key (kbd "M-z") 'zap-up-to-char)

(global-unset-key (kbd "C-z"))
(global-set-key (kbd "C-z C-z") 'my-suspend-frame)
(global-set-key (kbd "C-M-s") 'switch-to-shell)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Custom keybindings for Major modes (compile and grep wrangling)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar compile-start-buffer)
(defvar grep-start-buffer)

(defun my-compile-hook ()
  "Grab current buffer name and store to global variable prior to compile."
  (setq compile-start-buffer (buffer-name (other-buffer "*compilation*" t))))

(defun return-from-compile ()
  (interactive)
  (bury-buffer "*compilation*")
  (switch-to-buffer compile-start-buffer))

(defun return-from-grep ()
  (interactive)
  (bury-buffer "*grep*")
  (switch-to-buffer grep-start-buffer))

(add-hook 'compilation-mode-hook 'my-compile-hook)
(add-hook 'grep-mode-hook 'my-grep-hook)

(progn
  (require 'compile)
  ;; modify compilation keys
  ;;(define-key compilation-mode-map (kbd "q") 'special-bury-buffer))
  (define-key compilation-mode-map (kbd "q") 'return-from-compile))
  
(progn
  (require 'grep)
  ;; modify grep keys 
  (define-key grep-mode-map (kbd "q") 'return-from-grep))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Global mode settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-auto-revert-mode t)
(show-paren-mode t)
(scroll-bar-mode -1)
(menu-bar-mode -1)

(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))

; Example of settings based on OS type.
(when (not (string= system-type "darwin")) ; Mac
  ;; (set-face-attribute 'default nil :height 130)
  
; Example of settings based on host name
(when (string= (system-name) "lala1111.magicleap.ds")
  (set-face-attribute 'default nil
                      :family "Menlo"
                      :height 110
                      :weight 'normal
                      :width 'normal))

; Example of setting if in windowed mode
(unless (display-graphic-p)
  (menu-bar-mode -1))
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Custom setq variables
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq confirm-kill-emacs 'y-or-n-p)
(setq ring-bell-function 'ignore))
(setq inhibit-splash-screen t)
(setq column-number-mode t)
(setq mac-command-modifier 'meta)
(unless (display-graphic-p)
  (menu-bar-mode -1))

; Set backup directory (so we don't get all the *~ in the working directories)
(setq backup-directory-alist `(("." . "~/.saves"))) 
(setq backup-by-copying t)
(setq delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Custom functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun my-go-mode-before-save-hook ()
  (when (eq major-mode 'go-mode)
    (gofmt)))

(add-hook 'before-save-hook #'my-go-mode-before-save-hook)


(defun grab-current-filename()
  " Add the current buffer filename to the kill ring.
If buffer is not visiting a file, add the buffer name"
  (interactive)
  (let ((name (buffer-file-name (current-buffer))))
    (message name)
    (if name
	(kill-new name nil)
      (message "Buffer not visiting file. Filename not killed.")
      )
    )
  )

(defun save-copy-as (filename &optional confirm)
  "Write the current buffer to a file, without visiting that file"
  (interactive (list (if buffer-file-name
	     (read-file-name "Save a copy of file as: "
			     nil nil nil nil)
	   (read-file-name "Save a copy of buffer as: " default-directory
			   (expand-file-name
			    (file-name-nondirectory (buffer-name))
			    default-directory)
			   nil nil))
	 (not current-prefix-arg)))

  (message filename)
   (save-excursion
    (mark-wphole-buffer)
    (write-region nil nil filename))
   )

(defun align-repeat (start end regexp)
  "Repeat alignment with respect to 
   the given regular expression."
  (interactive "r\nsAlign regexp: ")
  (align-regexp start end 
		(concat "\\(\\s-*\\)" regexp) 1 1 t))

(defun my-suspend-frame ()
  "In a GUI environment, do nothing; otherwise `suspend-frame'."
  (interactive)
  (if (display-graphic-p)
      (message "suspend-frame disabled for graphical displays.")
    (suspend-frame)))

(defun switch-to-shell ()
  "Immediately jump to shell buffer, creating it if it does not exist."
  (interactive)
   (if (not (eq nil (get-buffer "*shell*")))
       (switch-to-buffer "*shell*")
     (shell)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Anything else
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Auto-indent yanked text
(dolist (command '(yank yank-pop))
  (eval `(defadvice ,command (after indent-region activate)
           (and (not current-prefix-arg)
                (member major-mode '(emacs-lisp-mode lisp-mode
                                                     clojure-mode    scheme-mode
                                                     haskell-mode    ruby-mode
                                                     rspec-mode      python-mode
                                                     c-mode          c++-mode
                                                     objc-mode       latex-mode
                                                     plain-tex-mode))
                (let ((mark-even-if-inactive transient-mark-mode))
                  (indent-region (region-beginning) (region-end) nil))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; EMACS Autoset stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "red3" "ForestGreen" "yellow3" "blue" "magenta3" "DeepSkyBlue" "gray50"])
 '(comint-terminfo-terminal "xterm-256color")
 '(compilation-always-kill t)
 '(compile-command "go build")
 '(custom-enabled-themes nil)
 '(desktop-restore-frames nil)
 '(desktop-save 'if-exists)
 '(desktop-save-mode t)
 '(display-line-numbers-width 3)
 '(flycheck-check-syntax-automatically '(save idle-change idle-buffer-switch mode-enabled))
 '(flycheck-disabled-checkers '(c/c++-clang))
 '(global-eldoc-mode nil)
 '(grep-setup-hook '((lambda nil (setq truncate-lines t))))
 '(helm-buffer-max-length 50)
 '(helm-completion-style 'emacs)
 '(helm-fuzzy-sort-fn 'helm-fuzzy-matching-sort-fn-preserve-ties-order)
 '(mode-require-final-newline nil)
 '(package-selected-packages
   '(py-autopep8 jedi-mode protobuf-mode exec-path-from-shell helm-lsp lsp-ui lsp-mode go-mode nix-mode magit helm-config cuda-mode flycheck-rtags helm-rtags projectile shackle matlab-mode flycheck lua-mode xclip yaml-mode web-mode use-package powerline org-bullets markdown-mode json-mode js2-mode glsl-mode dockerfile-mode csharp-mode cmake-mode))
 '(recentf-max-menu-items 25)
 '(recentf-max-saved-items 25)
 '(recentf-mode t)
 '(require-final-newline nil)
 '(tool-bar-mode nil)
 '(truncate-lines t)
 '(warning-suppress-log-types '((comp)))
 '(warning-suppress-types '((lsp-mode) (comp)))
 '(which-function-mode t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background "#282c34" :foreground "#dcdfe4" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 98 :width normal :foundry "nil" :family "Hack"))))
 '(font-lock-builtin-face ((t (:foreground "#61afef" :weight extra-bold))))
 '(font-lock-comment-face ((t (:foreground "firebrick2"))))
 '(font-lock-constant-face ((t (:foreground "#c678dd"))))
 '(font-lock-function-name-face ((t (:foreground "#61afef" :weight extra-bold))))
 '(font-lock-keyword-face ((t (:foreground "#56b6c2" :weight bold))))
 '(font-lock-string-face ((t (:foreground "#98c379"))))
 '(font-lock-type-face ((t (:foreground "#98c379" :weight bold))))
 '(font-lock-variable-name-face ((t (:foreground "#e5c07b"))))
 '(font-lock-warning-face ((t (:inherit error :weight normal))))
 '(helm-ff-directory ((t (:inherit font-lock-function-name-face))))
 '(helm-ff-file ((t (:foreground "#dcdfe4"))))
 '(helm-selection ((t (:underline "magenta"))))
 '(line-number ((t (:inherit (shadow default) :foreground "#98c379"))))
 '(line-number-current-line ((t (:inherit line-number :foreground "gold2" :weight bold))))
 '(powerline-active0 ((t (:inherit mode-line :background "lime green"))))
 '(region ((t (:background "#61afef" :foreground "#dcdfe4")))))
