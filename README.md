# John' Dotfiles

Linux configuration files

Needed to do this for 100 years.  Let's see if I can even marginally maintain it.

## Overview

I'm setting this whole thing up on my Framework laptop to run using NixOS.  NixOS should handle the entirety of the ssystem setup, while the dotfiles contained herein are intended for the user-level configuration.  Automating user-level setup will likely be an ongoing effort.  All other configuration directories in my repositories should be deprecated over the coming months.

## To clone to new system:

```
git clone --bare git@gitlab.com:johnmarianhoffman/dotfiles.git $HOME/.dotfiles
nixos-rebuild switch -I nixos/configuration.nix
```

