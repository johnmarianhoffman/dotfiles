# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000
unsetopt beep
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/john/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

alias dotfiles='git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias df2="dotfiles"
alias e="exit"
alias git-graph="git log --graph --oneline --decorate --all"
alias gg="git log --graph --oneline --decorate --all"
alias rebuild-nix="sudo nixos-rebuild switch -I nixos-config=/home/john/nixos/configuration.nix"
alias visualize="gource --auto-skip-seconds 1 -s 0.1"

export PATH=$PATH:$HOME/go/bin
